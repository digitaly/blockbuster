package digitaly.tech.domain

interface UseCase<Params, Source> {
    suspend fun execute(params: Params): Source

    abstract class NoParam<Source> : UseCase<None, Source> {
        abstract fun execute(): Source
        override suspend fun execute(params: None): Source = throw UnsupportedOperationException()
        operator fun invoke(): Source = execute()
    }

    object None
}